class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if verify_recaptcha(model: @contact, message: "reCAHPTCHA OK") && @contact.deliver
      redirect_to root_path, notice: 'Thank you for your letter'
    else
      render :new, notice: 'Sorry, a system error, please try again later'
    end
  end

end