class ApplicationController < ActionController::Base

  http_basic_authenticate_with name: "admin", password: "kverko_admin" if Rails.env.production?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_defaults

  
  # Sets defaults projects
  def set_defaults
    @last_projects = Project.limit(3).reverse_order
  end
end
