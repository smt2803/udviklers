class ProjectImage < ActiveRecord::Base
  belongs_to :project

  has_attached_file :image, :styles => { :original => "800x1300>", :medium => "300x700>", :thumb => "100x150>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates_attachment_presence :image , message: 'Image attached'
  validates_attachment_size :image, :less_than => 2.megabytes, message: 'Image has valid size'
end
