class Project < ActiveRecord::Base
  include Shared_scopes
  has_many :project_images
  
  validates :body, :title, :url, presence: true
  validates :body, length: {maximum: 65535}
  validates :title, length: {maximum: 255}
end
