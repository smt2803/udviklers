ActiveAdmin.register Project do

 permit_params :title, :body, :url, :updated_at
  
  index do  
    column :id do |project|  
      link_to project.id, admin_project_path(project)  
    end        
    column :title  
    column :body  do |project| 
      raw project.body
    end
    actions  
  end

  form do |f|
    f.inputs "Project" do
      f.input :title
      f.input :body, :as => :ckeditor, :input_html => { :ckeditor => {:toolbar => 'FULL'} }
      f.input :url
    end
    f.actions
  end
  
  show do
    attributes_table do
      row :id  
      row :title
      row :body do |page|
        raw page.body
      end
      row :url
    end
    active_admin_comments
  end

end
