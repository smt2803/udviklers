ActiveAdmin.register ProjectImage do

permit_params :project_id, :image

  index do
    column :project
    column :image do |c|
      link_to image_tag(c.image.url(:thumb)), c.image.url(:thumb)
    end
    actions
  end

  form do |f|
    f.inputs "Projects images" do
      f.input :project
      f.input :image, :required => true, :as => :file
    end
    f.actions
  end

  show do
    attributes_table do
      row :project
      row :image do |c|
        link_to image_tag(c.image.url(:thumb)), c.image.url
      end
    end
    active_admin_comments
  end

end
