class ProjectDecorator < Draper::Decorator
  delegate_all
  
  decorates_association :project_images

  # @return [image_tag] for first project image  
  def img_tag
    if project.project_images.empty?
      h.image_tag("http://placehold.it/250x200")
    else
      h.image_tag(project.project_images.first.image.url(:medium), size: "250x200")
    end
  end
end