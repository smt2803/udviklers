class ProjectImageDecorator < Draper::Decorator
  delegate_all

  
  # @return [image url] for project image
  def img_url(style)
    if object.nil?
      "http://placehold.it/250x200"
    else
      object.image.url(style)
    end
  end

  # @return [image_tag] for project image  
  def img_tag(style)
    h.image_tag(img_url(style))
  end
  
  # @return [link_tag] for project image  
  def img_link_tag
    project_id = object.nil? ? "" : object.project.id
    
    h.link_to(img_tag(:medium), 
              img_url(:original),  
              {rel: "prettyPhoto[gallery#{project_id}]"})
  end
end