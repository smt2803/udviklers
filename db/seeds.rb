include ActionView::Helpers
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# clear tables(что бы не дублировались с каждым seed)
AdminUser.destroy_all
ProjectImage.destroy_all
Project.destroy_all


# project Test1
Project.create(
  title: "Test1",
  url: "http://test.com",
  body: 
      %{
      <div class="content">
        <h4>
          Description:
        </h4>
        Test1 helps to implement personal projects. Whether a customer needs to paint his house, learn a new language or plan children's birthday party....
        <h4>
          Technologies:
        </h4>
        Ruby on Rails, MySQL, CSS, HTML5, Javascript, Coffeescript, jQuery, twitter bootstrap, haml, Apache+Passenger, Ubuntu, Twilio(texting), Stripe(payments), Sendgrid(mailing), ActiveAdmin(administrative), Rspec(testing), Capistrano(deploying), Git(version control)
        <h4>
          Environments:
        </h4>
        development, test, production, staging
      </div>
      }
)

# project Test2
Project.create(
  title: "Test2",
  url: "http://test.com",
  body: 
      %{
        <div class="content">
          <h4>
            Description:
          </h4>
          Test2 is a company that provides contracting and consulting services around the world...
          <h4>
            Technologies:
          </h4>
          Ruby on Rails, MySQL, CSS, HTML5, Javascript, Coffeescript, jQuery, twitter bootstrap, haml, Apache+Passenger, Ubuntu, ActiveAdmin(administrative), Rspec(testing), Capistrano(deploying), Git(version control), Sidekiq (jobs scheduling), Solr(searching)
          <h4>
            Environments:
          </h4>
          development, test, production
        </div>
      }      
)

# project Test3
Project.create(
  title: "Test3",
  url: "http://test.com",
  body: 
      %{
        <div class="content">
          <h4>
            Description:
          </h4>
          Test3 helps to advertise
          <h4>
            Technologies:
          </h4>
          Ruby on Rails, MySQL, CSS, HTML5, Javascript, Coffeescript, jQuery, twitter bootstrap, haml, Apache+Passenger, Ubuntu, ActiveAdmin(administrative), Rspec(testing), Capistrano(deploying), Git(version control), Sidekiq (jobs scheduling), Solr(searching)
          <h4>
            Environments:
          </h4>
          development, test, production, staging
        </div>
      }
)
# test AdminUser
admin_user = AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')