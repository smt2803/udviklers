class AddImageToProjectImage < ActiveRecord::Migration
  def self.up
    add_attachment :project_images, :image
  end

  def self.down
    remove_attachment :project_images, :image
  end
end
