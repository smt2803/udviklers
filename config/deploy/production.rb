server '54.213.141.45', user: 'ubuntu', roles: %w{app db web}, primary: true

role :app, %w{ubuntu@54.213.141.45}
role :web, %w{ubuntu@54.213.141.45}
role :db,  %w{ubuntu@54.213.141.45}

set :ssh_options, {
  keys: %w(/home/smt/.ssh/amazonkey.pem),
  forward_agent: true,
  auth_methods: %w(publickey password),
 }