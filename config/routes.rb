Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  root to: 'visitors#index'
  resources :projects, only: [:index] do
    get '/page/:page', to: 'projects#index', on: :collection 
    
  end
  resources :contacts, only: [:new, :create]
end
