# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'Kverko'
set :repo_url, 'git@gitlab.com:smt2803/udviklers.git'

# Default branch is :master
set :branch, 'development'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/ubuntu/udviklers'
set :deploy_user, 'ubuntu'


# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/aws.yml .env}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}


#hipchat notification setting
set :hipchat_token, "485a5039450e715e5f7d7cec1acb61"
set :hipchat_room_name, "udviklers.com" # If you pass an array such as ["room_a", "room_b"] you can send announcements to multiple rooms.
# Optional
set :hipchat_enabled, true # set to false to prevent any messages from being sent
set :hipchat_announce, false # notify users
set :hipchat_color, 'yellow' #normal message color
set :hipchat_success_color, 'green' #finished deployment message color
set :hipchat_failed_color, 'red' #cancelled deployment message color

namespace :deploy do

  desc 'Restart application'
  task :restart do 
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc "Seed the database."
  task :seed_db do
    on roles(:app) do
      within "#{release_path}" do
        execute :rake, "db:seed RAILS_ENV=#{fetch :stage}"
      end
    end
  end
  before 'deploy:publishing', 'deploy:seed_db'
  after :publishing, :restart
end